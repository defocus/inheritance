﻿using System;

namespace Task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Printer print = new Printer();
            
            print.Print($"Type this exemplar: {print.GetType()}\n\n");

            Printer test = new Test();

            test.Print($"Type this exemplar: {test.GetType()}\n\n");
        }
    }
}
