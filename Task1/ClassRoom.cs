﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class ClassRoom
    {
        static Pupil[] _pupil;
                
        public ClassRoom(int index)
        {
            _pupil = new Pupil[index];

            InicializationClassRoom();
        } 
        
        static void InicializationClassRoom()
        {
            Random rand = new Random();

            for (int i = 0; i < _pupil.Length; i++)
            {
                _pupil[i] =
                    rand.Next(1, 3) switch
                    {
                        1 => new ExelentPupil(),
                        2 => new BadPupil(),
                        3 => new GoodPupil(),
                        _ => null
                    };
            }
        }

        public void ShowClassRoom()
        {
            for (int i = 0; i < _pupil.Length; i++)
            {
                _pupil[i].Study();
                _pupil[i].Read();
                _pupil[i].Write();
                _pupil[i].Relax();
                Console.WriteLine("-------------------------------------------------------\n");
            }
        }
    }
}
