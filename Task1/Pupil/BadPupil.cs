﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class BadPupil : Pupil
    {
        public override void Read()
        {
            Console.WriteLine("This student doesn't read well...");
        }

        public override void Relax()
        {
            
            Console.WriteLine("This student amazing relax...");
        }

        public override void Study()
        {
            Console.WriteLine("This student doesn't study well...");
        }

        public override void Write()
        {
            Console.WriteLine("This student doesn't write well...");
        }
    }
}
