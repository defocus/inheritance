﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    internal class GoodPupil : Pupil
    {
        public override void Read()
        {
            Console.WriteLine("This student nice reading...");
        }

        public override void Relax()
        {
            Console.WriteLine("This student good relaxing...");
        }

        public override void Study()
        {
            Console.WriteLine("This student nice studing...");
        }

        public override void Write()
        {
            Console.WriteLine("This student good writing...");
        }
    }
}
