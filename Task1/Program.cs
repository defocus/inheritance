﻿using System;

namespace Task1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter pupil in the class: ");

            ClassRoom _class = new ClassRoom(int.Parse(Console.ReadLine()));

            _class.ShowClassRoom();
        }
    }
}