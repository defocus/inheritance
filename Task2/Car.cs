﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Car : Vehicle
    {
        public override string Price { get => "100 000 dollars"; set => base.Price = value; }
        public override string Speed { get => "Max speed - 260 km/h"; set => base.Speed = value; }
        public override string YearIssuse { get => "2021 year of issuse"; set => base.YearIssuse = value; }
        public override string Coordinates { get => "Somewhere in Ukraine"; set => base.Coordinates = value; }
    }
}
