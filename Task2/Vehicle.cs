﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Vehicle
    {
        public virtual string Price { get; set; }
        public virtual string Speed { get; set; }
        public virtual string YearIssuse { get; set; }
        public virtual string Coordinates { get; set; }
        public virtual string NumPeople { get; set; }
        public virtual string Port { get; set; }
    }
}
