﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Plane : Vehicle
    {
        
        public override string Price { get => "1 billion dollars"; set => base.Price = value; }
        public override string Speed { get => "Optimal speed - 800 km/h"; set => base.Speed = value; }
        public override string YearIssuse { get => "2018 year of issuse"; set => base.YearIssuse = value; }
        public override string Coordinates { get => "10000 meters above sea level"; set => base.Coordinates = value; }
        public override string NumPeople { get => "400 people on board"; }
    }
}
