﻿using System;

namespace Task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Vehicle[] vehicle = new Vehicle[3];
            
            vehicle[0] = new Car();
            vehicle[1] = new Plane();
            vehicle[2] = new Ship();

            for (int i = 0; i < vehicle.Length; i++)
            {
                Console.WriteLine(vehicle[i].Price);
                Console.WriteLine(vehicle[i].Speed);
                Console.WriteLine(vehicle[i].YearIssuse);
                Console.WriteLine(vehicle[i].Coordinates);

                if(vehicle[i].GetType() == typeof(Plane))
                {
                    Console.WriteLine(vehicle[i].NumPeople);
                }

                if (vehicle[i].GetType() == typeof(Ship))
                {
                    Console.WriteLine(vehicle[i].NumPeople);
                    Console.WriteLine(vehicle[i].Port);
                }

                Console.WriteLine("-------------------------------\n");
            }
        }
    }
}
