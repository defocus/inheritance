﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class Ship : Vehicle
    {
        public override string Price { get => "1 million dollars"; set => base.Price = value; }
        public override string Speed { get => "30 knots per hours"; set => base.Speed = value; }
        public override string YearIssuse { get => "2020 year of issuse"; set => base.YearIssuse = value; }
        public override string Coordinates { get => "Somewere in Pacific Ocean"; set => base.Coordinates = value; }
        public override string Port { get => "Home port: Tortuga"; }
        public override string NumPeople { get => "1400 people on board"; }
    }
}
