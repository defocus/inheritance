﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class ProDocumentWorker : DocumentWorker
    {
        public override void EditDocument()
        {
            Console.WriteLine("Document modified...");
        }

        public override void SaveDocument()
        {
            Console.WriteLine("Document saved in old format, save in all format allow in EXPERT version");
        }
    }
}
