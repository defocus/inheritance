﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    internal class DocumentWorker
    {
        public virtual void OpenDocument()
        {
            Console.WriteLine("Document open...");
        }

        public virtual void EditDocument()
        {
            Console.WriteLine("Edit documet allowed in PRO version...");
        }

        public virtual void SaveDocument()
        {
            Console.WriteLine("Save document allowed in PRO version...");
        }
    }
}
