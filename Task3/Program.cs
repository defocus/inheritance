﻿using System;

namespace Task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter key for PRO or EXPERT version: ");
            //PRO - 1111
            //EXPERT - 1234

            string key = Console.ReadLine();

            DocumentWorker document = key switch
                    {
                        "1111" => new ProDocumentWorker(),
                        "1234" => new ExpertDocumentWorker(),
                        _ => new DocumentWorker()
                    };

            document.OpenDocument();
            document.EditDocument();
            document.SaveDocument();

            Console.ReadKey();
        }
    }
}
